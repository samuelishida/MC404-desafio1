.syntax unified
.text
.align 2
.global main
main:
    push {lr}
    
    bl _scan											@ Chama rotina que le valor de M
    ldr r2, =M											@ r2 recebe endereco de M
    ldr r2, [r2]										@ r2 recebe valor de M
    cmp r2, #1											@ compara r2 e 1
    bls main_end										@ se for menor ou igual, termina o programa
    mov r3, #10											@ constante de multiplicacao
    mov r1, #1											@ r1 recebe 1
	
	gen_extremes:
		mul r1, r1, r3									@ multiplica r1 por 10 
		sub r2, #1										@ subtrai de r2 1
		cmp r2, #1										@ compara r2 e 1
		beq main_continue								@ ja calculou os extremos
		b gen_extremes									@ se nao continua a interar  
	
	main_continue:
		mul r1, r1, r3									@ multiplica r1 por 10
		sub r1, #1										@ subtrai de r1 1
		
		mov r5, r1										@ r5 recebe r1 como auxiliar
		
		mov r0, #0										@ parametro NULL para time
   		bl 	time										@ time devolve o clock em segundos desde a "epoca"
   		mov r7, r0										@ salva clock(segundos desde "epoca")
   		
   		mov r0, #0										@ r0 recebe 0
   		mov r1, r5										@ r1 recebe auxiliar r5 que armazenou valor de r1 antes da rotina time
   		
		verify_all_numbers:								@ verifica todas possibilidade (ate r1)
			bl _find_kaprekar_number 					@ rotina q procura o numero magico
			add r0, #1									@ incrementa-se r0
			cmp r0, r1									@ compara r0 e r1
			beq main_end								@ se forem iguais, termina programa
			b verify_all_numbers						@ se nao, continua procurar numeros magicos
    
   main_end:
		bl _print_total_converge						@ mostra conversões
		
		mov r0, #0    									@ parametro NULL
  		bl	time										@ time devolve o clock em segundos desde a "epoca"
  		mov r8, r0										@ r8 recebe r0, que e o retorno de time
 		sub r0, r8, r7 									@ r0= elapsed time (segs)
   		bl _print_time     								@ exibe elapsed time (= 2 segs)
		
    	pop {pc}
    
/***************************************************************************************/
@ Rotina que encontra o numero de Kaprekar
@ Inputs:
@	r0 - numero a ser verificado

_find_kaprekar_number:
	push {r0-r11, lr}									@ salva variaveis na pilha
	ldr r6, =circle_array_size							@ r6 recebe apontador para tamanho do vetor circular
	mov r7, #0											@ r7 guardara o valor encontrado por interacao
	str r7, [r6]										@ a cada numero a ser procurado, tamanho do vetor eh 0
	
	fkn_loop:
		bl _extract_digits								@ rotina que extrai os digitos de um numero
		ldr r0, =digits_array							@ r0 recebe apontador para inicio do vetor
		ldr r1, =M										@ r1 recebe apontador para tamanho do vetor
		ldr r1, [r1]									@ r1 recebe tamanho do vetor
		bl _sort_asc									@ ordena o vetor dos digitos crescentemente
		bl _generate_min_number							@ rotina que gera o menor numero
		mov r4, r0										@ r4 recebe menor numero
		bl _generate_max_number							@ rotina que gera o maior numero
		mov r5, r0										@ r5 recebe maior numero			
		sub r0, r5, r4									@ r0 recebe r4 menos r5
		cmp r0, #0										@ compara r0 e 0
		bls fkn_end										@ se for menor ou igual, termina (nao achou numero kaprekar)
		cmp r0, r7										@ compara r0 e r7 (numero calculado anteriormente)
		beq find_kaprekar								@ se forem iguais, achou numero magico
		mov r7, r0										@ r7 recebe r0
		bl _verify_converge_number						@ verifica se o numero eh circular e nao converge
		mov r11, r0										@ r11 recebe r0
		mov r0, r7										@ r0 recebe auxiliar r7
		cmp r11, #0										@ compara retorno da rotina se eh falso
		beq fkn_end										@ se for, termina a rotina
		b fkn_loop										@ se nao, volta a interar
		
	find_kaprekar:
		ldr r8, =kaprekar_qtd							@ carrega em r8 endereco da qtd dos encontrados
		ldr r9, [r8]									@ carrega em r9 o valor contido no endereco r8
		add r9, #1										@ incrementa r9
		str r9, [r8]									@ atualiza constante kaprekar_qtd
		bl _add_kaprekar_in_array						@ rotina que insere o numero magico no vetor de kaprekar
		
	fkn_end:
    	pop {r0-r11, pc}       							@ restora dados da pilha e sai da subrotina
    
   
/***************************************************************************************/
@ Rotina que extrai os digitos de um numero
@ Inputs:
@ 	r0 - numero a ser extraido os digitos

_extract_digits:
	push {r0-r11, lr}									@ salva variaveis na pilha
	ldr r4, =digits_array 								@ r4 recebe ponteiro para o inicio do vetor
	mov r5, r0											@ salva r0 em variavel temporaria
	mov r9, #10											@ constante 10
	mov r10, #0											@ auxiliar que representa numero de digitos encontrados
	
	ldr r11, =M											@ r11 recebe endereco de M
	ldr r11, [r11]										@ r11 recebe valor contido no endereco
	clean_digits_array:
		str r10, [r4], #4								@ zera vetor de digitos
		sub r11, #1										@ atualizar contador r11
		cmp r11, #0										@ compara r11 e 0
		bne clean_digits_array							@ continua zerar o vetor enquanto contador nao for 0
		
	ldr r4, =digits_array 								@ r4 recebe ponteiro para o inicio do vetor	
	
	ed_loop:
		udiv r6, r5, r9									@ r6 recebe r5 / 10
		mul r7, r6, r9									@ r7 recebe r6 * 10
		sub r8, r5, r7									@ r8 recebe r5 - r7 (obtemos o resto da divisao)
		udiv r5, r5, r9									@ atualizamos r5 (r5 / 10)
 		str r8, [r4], #4								@ guarda r8 no vetor e incrementa r4 	
 		cmp r5, #0										@ compara r5 e zero
 		bne	ed_loop										@ se for diferente, continua dividindo
    	
    finish_ex_false:
    	pop {r0-r11, pc}       							@ restora dados da pilha e sai da subrotina
    
/***************************************************************************************/
@ Rotina que gera o maior numero inteiro a partir do vetor de digitos
@ Output:
@	r0 - numero gerado

_generate_max_number:
	push {r4-r11, lr}									@ salva variaveis na pilha
	mov r0, #0											@ inicializa o numero a ser gerado
	mov r7, #10											@ constante de multiplicacao
	ldr r4, =digits_array								@ r4 recebe ponteiro para inicio do vetor
	ldr r5, =M											@ r5 recebe apontador para numero de digitos (tamanho do vetor)
	ldr r5, [r5]										@ r5 recebe numero de digitos
	
	mov r8, r5											@ r8 recebe r5
	fix_array_pointer:									@ loop que leva ponteiro para fim do vetor
		add r4, r4, #4									@ r4 = r4 + 4
		sub r8, #1										@ subtrai 1 de r8
		cmp r8, #1										@ compara r8 e 1
		bne fix_array_pointer							@ se nao for igual, continua a posicionar o ponteiro
	
	gmaxn_loop:
		ldr	r6, [r4], #-4								@ r6 recebe elemento do vetor (ja eh o menos significativo)
		mul r0, r0, r7									@ multiplica r0 por 10
		add r0, r0, r6									@ resultado final: r0 = r0*10 + r6
		sub r5, #1										@ decrementa contador r5
		cmp r5, #0										@ compara r5 e 0
		bne gmaxn_loop									@ se forem diferentes, continua o processo
	
	pop {r4-r11, pc}       								@ restora dados da pilha e sai da subrotina

/***************************************************************************************/
@ Rotina que gera o menor numero inteiro a partir do vetor de digitos
@ Output:
@	r0 - numero gerado

_generate_min_number:
	push {r4-r11, lr}									@ salva variaveis na pilha
	mov r0, #0											@ inicializa o numero a ser gerado
	mov r7, #10											@ constante de multiplicacao
	ldr r4, =digits_array								@ r4 recebe ponteiro para inicio do vetor
	ldr r5, =M											@ r5 recebe apontador para numero de digitos (tamanho do vetor)
	ldr r5, [r5]										@ r5 recebe numero de digitos
	
	gmn_loop:
		ldr	r6, [r4], #4								@ r6 recebe elemento do vetor (ja eh o menos significativo)
		mul r0, r0, r7									@ multiplica r0 por 10
		add r0, r0, r6									@ resultado final: r0 = r0*10 + r6
		sub r5, #1										@ decrementa contador r5
		cmp r5, #0										@ compara r5 e 0
		bne gmn_loop									@ se forem diferentes, continua o processo
		
    pop {r4-r11, pc}       								@ restora dados da pilha e sai da subrotina
    
/***************************************************************************************/
@ Rotina que insere um elemento nao repetido em um vetor  
@ Inputs:
@	r0 - numero magico a ser inserido
    
_add_kaprekar_in_array:
	push {r0-r11, lr}									@ salva variaveis na pilha
	ldr r4, =kaprekar_array			                    @ r4 recebe ponteiro para inicio do vetor de kaprekar
	ldr r5, =kaprekar_array_size						@ r5 recebe tamanho do vetor de kaprekar
	ldr r8, [r5]										@ carrega em r8 o valor contido no ponteiro
	cmp r8, #0											@ compara r5 e zero
	beq add_at_end										@ se for igual, insere no vetor
	
	akia_loop:
		ldr r6, [r4], #4                      			@ carrega elemento do vetor
		cmp r0, r6										@ compara r2 e r6
		beq akia_end									@ se for igual, nao insere
		sub r8, #1										@ decrementa tamanho do vetor
		cmp r8, #0										@ compara r5 e 1
		bne akia_loop									@ continua a procurar
		
	akia_insert:
		add_at_end:
		
			mov r11, r0									@ r11 recebe r0 como auxiliar
			ldr r0, =prtmsg								@ r0 recebe endereco na msg a ser printada
			mov r1, r11									@ r1 recebe valor a ser exibido na tela
			bl printf									@ chama rotina de printa
			mov r0, r11									@ atualiza novamente r0
		
			str r0, [r4]								@ guarda valor no vetor de kaprekar
			ldr r8, [r5]
			add r8, r8, #1								@ incrementa r1
			str r8, [r5]								@ atualiza tamanho do vetor de kaprekar
	
	akia_end:
		pop {r0-r11, pc}       							@ restora dados da pilha e sai da subrotina
		
/***************************************************************************************/
@ Rotina que verifica se um numero vai convergir ou não
@ Inputs:
@	r0 - numero a ser verificado
@ Outputs:
@	r0 - true ou false
		
_verify_converge_number:
	push {r1-r11, lr}									@ salva variaveis na pilha
	mov r4, r0											@ r4 recebe numero a ser verificado
	ldr r5, =circle_array								@ r5 recebe ponteiro para vetor circular
	ldr r6, =circle_array_size							@ r6 recebe ponteiro para o tamanho do vetor circular
	ldr r6, [r6]										@ r6 recebe valor contido no endereco
	cmp r6, #0											@ se r6 for 0, ja podemos inserir
	beq vcn_insert_true									@ chama rotulo que insere
	
	vcn_loop:
		ldr r7, [r5], #4								@ carrega em r7 um elemento do vetor circular
		cmp r7, r4										@ compara r7 e o valor recebido na rotina
		beq vcn_end_false								@ se for igual, sabemos nao ira convergir. Retorna falso
		sub r6, #1										@ atualiza contador do loop
		cmp r6, #0										@ compara contador e 0
		bne vcn_loop									@ enquanto nao for 0, continua loop
	
	vcn_insert_true:
		str r4, [r5]									@ insere valor r4 no vetor circular
		ldr r6, =circle_array_size						@ r6 recebe apontador para o tamanho deste vetor
		ldr r7, [r6]									@ r7 recebe valor contido no endereco em r6
		add r7, #1										@ incrementa-se o tamanho do vetor circular
		str r7, [r6]									@ guarda valor em r6
		mov r0, #1										@ retorna true
		pop {r1-r11, pc}       							@ restora dados da pilha e sai da subrotina

	vcn_end_false:
		mov r0, #0										@ retorna false
		pop {r1-r11, pc}       							@ restora dados da pilha e sai da subrotina

/***************************************************************************************/
@ Rotina que ordena um vetor. Baseada no algoritimo bubble-sort
@ Inputs:
@	r0 - ponteiro para inicio do vetor
@	r1 - tamanho do vetor

_sort_asc:
	push {r0-r11, lr}									@ salva variaveis na pilha
	cmp r1, #1                                          @ caso vetor possua tamanho 1
	ble end_sort                                      	@ encerra-se a rotina

	sub r5, r1, #1                                      @ numero de comparacoes: n-1
 	mov r4, r0			                                @ r4 recebe ponteiro para inicio do vetor
 	mov r6, #0                                         	@ r6 eh setado quando ocorre swap

	loop_start:
 		ldr r7, [r4], #4                      			@ carrega elemento do vetor
 		ldr r8, [r4]                                    @ carrega proximo elemento
 		cmp r7, r8                                      @ compara ambos
 		ble no_swap                                   	@ se o segundo for maior, nao realiza o swap

 	mov r6, #1                                       	@ seta flag, pois ocorrera swap
 	sub r4, r4, #4                          			@ reseta ponteiro
 					 									@ swp r8, r8, [r4] - realiza swap
 	ldr r9, [r4]										@ r9 auxiliar recebe valor apontado por r4
 	str r8, [r4]										@ endereco apontado por r4 recebe r8
 	mov r8, r9											@ r8 recebe r9 - swap realizado						
 	str r8, [r4, #4]!                         			@ adiciona no vetor r8
 	
	no_swap:
 		subs r5, r5, #1                                 @ contador decrementado
		bne loop_start                                  @ restarta loop caso seja preciso

	end_inner:
 		cmp r6, #0                                      @ verifica flag
 		beq end_sort                                    @ finaliza rotina caso flag nao estiver setada

 	mov r6, #0                                         	@ limpa flag
 	ldr r4, =digits_array                               @ r4 recebe ponteiro para inicio do vetor
 	sub r5, r1, #1                                      @ reseta contador
 	b loop_start                                       	@ intera novamente

	end_sort:
        pop {r0-r11, pc}       							@ restora dados da pilha e sai da subrotina
              
/***************************************************************************************/
@ Rotina que realiza o scan do valor de M

_scan:
    push {r0-r2, lr}    								@ salva variaveis na pilha
    ldr r0, =inmsg  									@ r0 recebe mensagem a ser exibida no monitor
	bl printf											@ printa-se a mensagem
	ldr r1, =M											@ r1 recebe constante M
	ldr r0, =scan_format								@ r0 recebe formato do scanf
	bl scanf											@ chama rotina que le o valor
    pop {r0-r2,pc}										@ restora dados da pilha e sai da subrotina
    
/***************************************************************************************/
@ Rotina que imprime o vator de kaprekar

_print_kaprekar_array:
	push {r0-r11, lr}    								@ salva variaveis na pilha
	ldr r4, =kaprekar_array_size						@ carrega em r4 ponteiro do tamanho do vetor de kaprekar
	ldr r4, [r4]										@ r4 recebe tamanho do vetor de kaprekar
	cmp r4, #0											@ compara r4 e 0
	beq pka_end											@ se for igual, nao printa nada
	ldr r5, =kaprekar_array			                    @ r5 recebe ponteiro para inicio do vetor de kaprekar
	
	pka_loop:
		ldr r0, =prtmsg										@ r0 recebe mensagem a ser printada
		ldr r1, [r5], #4                      				@ r1 recebe elemento do vetor
		bl printf											@ printa valor
		sub r4, #1											@ subtrai 1 do contador
		cmp r4, #0											@ compara r4 e 0
		beq pka_loop										@ se nao forem iguais, continua a interar

	pka_end:
   		pop {r0-r11,pc}										@ restora dados da pilha e sai da subrotina
 
 /***************************************************************************************/
@ Rotina que imprime o vetor de kaprekar
  		
_print_total_converge:
	push {r0-r11, lr}    								@ salva variaveis na pilha
		ldr r0, =prtmsg2								@ r0 recebe mensagem a ser printada
		ldr r1, =kaprekar_qtd							@ r1 recebe endereco dos numeros que convergiram
		ldr r1, [r1]									@ r1 recebe valor contido no endereco
		bl printf										@ chama rotina que printa
    pop {r0-r11,pc}										@ restora dados da pilha e sai da subrotina
    
 /***************************************************************************************/
@ Rotina que imprime o tempo de execucao
@ Input:
@	r0 - tempo de execucao
    
_print_time:	
   	push {r0-r11,lr}									@ salva variaveis na pilha
   	mov r1, r0											@ r1 recebe valor a ser mostrado na tela (tempo de execucao)
   	ldr r0, =fmt										@ r0 recebe endereco da string a ser mostrada
   	bl printf											@ chama rotina que printa
	pop {r0-r11,pc}										@ restora dados da pilha e sai da subrotina
	
/***************************************************************************************/
@ Secao de formatacoes
        
	scan_format:			.asciz "%d"
	inmsg: 					.asciz "\nInsira o número de dígitos M: "
	prtmsg:					.asciz "\nNumero magico detectado: %d"
	prtmsg2:				.asciz "\nTotal de numero que convergiram: %d \n"
	fmt: 					.asciz "Tempo de execucao = %d \n"

/***************************************************************************************/
@ Secao de dados

.data
.align	

	kaprekar_array:			.skip 100						@ vetor que guarda os numeros magicos encontrados	
	digits_array:			.skip 100						@ vetor que guarda os digitos do numero analisado
	circle_array:			.skip 500						@ vetor que guarda elementos calculados para cada numero
	steps:					.word 50						@ numero de passos do algoritimo
	M: 						.word 0							@ numero de digitos
	kaprekar_qtd:    		.word 0							@ numeros magicos encontrador
	kaprekar_array_size: 	.word 0							@ tamanho do vetor de kaprekar
	circle_array_size:		.word 0							@ tamanho do vetor circular
			
.end
