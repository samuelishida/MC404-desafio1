
.syntax unified
.align
.text
.global main

/***************************************************************************************/
@ Funcao principal

main:
    push {lr}
   	
   	mov r10, #32
   	   	
  	main_loop:
  		ldr r0, =timestr
  		bl _soma1
  		bl _print_time
   		sub r10, #1
   		cmp r10, #0
   		bne main_loop
    
    pop  {pc}
    
/***************************************************************************************/
@ Rotina que printa    

_print_time: 
    push {lr}
    ldr r1, =timestr
    ldr r0, =fmt
    bl 	printf
    pop {pc}
    
/***************************************************************************************/
@ Rotina que restora a palavra
    
_soma1:
	push {r0-r11, lr}
        
    ldrb r1, [r0, #7]!
    
    add_seconds:
    	cmp r1, #57
    	beq fix_seconds
    	add r1, #1
    	str r1, [r0]
    	b end_soma1
    
    fix_seconds:
    	ldrb r1, [r0, #-1]!    	
    	cmp r1, #53
    	beq fix_minutes_1
	  	add r1, #1
    	str r1, [r0]
    	mov r1, #48
    	str r1, [r0, #1]!
    	b end_soma1
    	
    fix_minutes_1:
    	ldrb r1, [r0, #-2]!
    	cmp r1, #57
    	beq fix_minutes_2
    	add r1, #1
    	str r1, [r0]
    	mov r2, #58
    	str r2, [r0, #1]!
    	mov r1, #48
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	b end_soma1
    	
    fix_minutes_2:
    	ldrb r1, [r0, #-1]!    	
    	cmp r1, #53
    	beq fix_hour_1
    	add r1, #1
    	str r1, [r0]
    	mov r1, #48
    	str r1, [r0, #1]!
    	mov r2, #58
    	str r2, [r0, #1]!
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	b end_soma1
    	
    fix_hour_1:    	
    	ldrb r1, [r0, #-3]!
    	cmp r1, #50
    	beq	fix_hour_situation_1
    	ldrb r1, [r0, #1]!
    	cmp r1, #57
    	beq fix_hour_2
    	b continue_fix_hour_1
    	
    	fix_hour_situation_1:
    		ldrb r1, [r0, #1]!
    		cmp r1, #51
    		beq fix_hour_2
    		
    	continue_fix_hour_1:
    		add r1, #1
    		str r1, [r0]
    		mov r2, #58
    		str r2, [r0, #1]!
    		mov r1, #48
    		str r1, [r0, #1]!
    		str r1, [r0, #1]!
    		str r2, [r0, #1]!
    		str r1, [r0, #1]!
    		str r1, [r0, #1]!
    		b end_soma1
    	
    fix_hour_2:
    	ldrb r1, [r0, #-1]!    	
    	cmp r1, #50
    	beq fix_hour_3
    	add r1, #1
    	str r1, [r0]
    	mov r1, #48
    	str r1, [r0, #1]!
    	mov r2, #58
    	str r2, [r0, #1]!
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	str r2, [r0, #1]!
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	b end_soma1
    	
    fix_hour_3:
    	sub r0, #1
    	mov r1, #48
    	mov r2, #58
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	str r2, [r0, #1]!
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	str r2, [r0, #1]!
    	str r1, [r0, #1]!
    	str r1, [r0, #1]!
    	b end_soma1
    
    end_soma1:
    	pop {r0-r11, pc}
 
/***************************************************************************************/
@ Secao de formatacoes   
    
	fmt: 			.asciz 		"%s\n"
	fmt_c:			.asciz		"%d\n"
	
/***************************************************************************************/
@ Secao de dados

.data
.align

	timestr:		.asciz		"23:59:48"
