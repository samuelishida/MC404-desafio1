
.syntax unified
.align
.text
.global main

/***************************************************************************************/
@ Definicao da macr0

.macro bfii addr,field,sbit,nbits				@ computes the absolute value of integer in register a
	ldr r4, =\field
	ldr r4, [r4]
	ldr r5, =\addr
	ldr r6, [r5]
	mov r11, #0xffffffff
	lsl r8, r11, #\nbits
	eor r8, r11
	and r4, r4, r8	
	ldr r9, =\sbit
	lsl r4, r4, r9	
	lsl r7, r11, #\nbits
	eor r7, r11
	lsl r7, r9
	bic r6, r6, r7
	orr r6, r6, r4
	str r6, [r5]
.endm

/***************************************************************************************/
@ Funcao principal

main:
    push {lr}
   	
   	@ Vamos exibir no video a palavra inicial
   												
    bl _print_word								
    
   	bfii word,field,s0,8
 	bl _print_word
    bl _restore_word
    
    bfii word,field,s4,8
    bl _print_word
    bl _restore_word
    
    bfii word,field,s8,8
    bl _print_word
    bl _restore_word
    
    bfii word,field,s12,8
    bl _print_word
    bl _restore_word
    
    bfii word,field,s16,8
    bl _print_word
    bl _restore_word
    
    bfii word,field,s20,8
    bl _print_word
 	bl _restore_word
    
    bfii word,field,s24,8
    bl _print_word
    bl _restore_word
    
    pop  {pc}
    
/***************************************************************************************/
@ Rotina que printa    

_print_word: 
    push {lr}
    ldr r1, =word
    ldr r1, [r1]
    ldr r0, =fmt
    bl 	printf
    pop {pc}
    
/***************************************************************************************/
@ Rotina que restora a palavra
    
_restore_word:
	push {lr}
    ldr r0, =word_fix
    ldr r0, [r0]
    ldr r1, =word
    str r0, [r1]
    pop {pc}
 
/***************************************************************************************/
@ Secao de formatacoes   
    
	fmt: .asciz "%08x\n"
	
/***************************************************************************************/
@ Secao de constantes (#define)
	
	.equ s0, 0
	.equ s4, 4
	.equ s8, 8
	.equ s12, 12
	.equ s16, 16
	.equ s20, 20
	.equ s24, 24
	
/***************************************************************************************/
@ Secao de dados

.data
.align

	word: 			.word 0xaabbccdd
	word_fix:		.word 0xaabbccdd
	field:			.word 0x9abcdf33
