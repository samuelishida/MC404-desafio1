.syntax unified
.text
.align 2
.global main
main:
    push {lr}
    
    bl _scan											@ Chama rotina que le valor de M
    mov r2, r0											@ r2 recebe M
    cmp r2, #1											@ compara r2 e 1
    bls main_end										@ se for menor ou igual, termina o programa
    mov r0, #1											@ r0 recebe 1
    mov r1, #1											@ r1 recebe 1
	
	gen_extremes:
		mul r0, r0, #10									@ multiplica r0 por 10    
		mul r1, r1, #10									@ multiplica r1 por 10 
		sub r2, #1										@ subtrai de r2 1
		cmp r2, #1										@ compara r2 e 1
		beq main_continue								@ ja calculou os extremos  
	
	main_continue:
		mul r1, r1, #10									@ multiplica r1 por 10
		sub r1, #1										@ subtrai de r1 1
		
		verify_all_numbers:								@ verifica todas possibilidade (ate r1)
			bl _find_kaprekar_number 					@ rotina q procura o numero magico
			add r0, #1									@ incrementa-se r0
			cmp r0, r1									@ compara r0 e r1
			beq main_end								@ se forem iguais, termina programa
			b verify_all_numbers						@ se nao, continua procurar numeros magicos
    
    main_end:
		bl _print_kaprekar_array						@ rotina que imprime todos valores do vetor de kaprekar
    	pop {pc}
    
/***************************************************************************************/
@ Rotina que encontra o numero de Kaprekar
@ Inputs:
@	r0 - numero a ser verificado

_find_kaprekar_number:
	push {r4-r11, lr}									@ salva variaveis na pilha
	ldr r6, =steps										@ r6 recebe ponteiro para numero de passos
	ldr r6, [r6]										@ r6 recebe valor
	mov r7, #0											@ r7 guardara o valor encontrado por interacao
	
	fkn_loop:
		sub r6, #1										@ subtrai contador de passos
		bl _extract_digits								@ rotina que extrai os digitos de um numero
		ldr r0, =digits_array							@ r0 recebe apontador para inicio do vetor
		ldr r1, =M										@ r1 recebe apontador para tamanho do vetor
		ldr r1, [r1]									@ r1 recebe tamanho do vetor
		bl _sort_asc									@ ordena o vetor dos digitos crescentemente
		bl _generate_max_number							@ rotina que gera maior numero
		mov r4, r0										@ r4 recebe maior numero
		bl _generate_max_number							@ rotina que gera menor numero
		mov r5, r0										@ r5 recebe menor numero
		sub r0, r4, r5									@ r0 recebe r4 menos r5
		cmp r0, #0										@ compara r0 e 0
		beq fkn_end										@ se forem iguais, termina (nao achou numero kaprekar)
		cmp r0, r7										@ compara r0 e r7 (numero calculado anteriormente)
		beq find_kaprekar								@ se forem iguais, achou numero magico
		cmp r6, #0										@ compara contador de passos e 0
		beq fkn_end										@ se forem iguai, termina (nao achou numero kaprekar)
		b fkn_loop										@ se nao, volta a interar
		
	find_kaprekar:
		ldr r8, =kaprekar_qtd							@ carrega em r8 endereco da qtd dos encontrados
		ldr r9, [r8]									@ carrega em r9 o valor contido no endereco r8
		add r9, #1										@ incrementa r9
		str r9, [r8]									@ atualiza constante kaprekar_qtd ***** VERIFICAR *****	
		mov r2, r0										@ recebe numero a ser inserido no vetor de kaprekar
		ldr r0, =kaprekar_array							@ r0 recebe apontador para inicio do vetor de kaprekar
		ldr r1, =kaprekar_array_size					@ r1 recebe apontador para tamanho do vetor de kaprekar 
		ldr r1, [r1]									@ r1 recebe tamanho do vetor de kaprekar
		bl _add_kaprekar_in_array						@ rotina que insere o numero magico no vetor de kaprekar
		
	fkn_end:
    	pop {r4-r11, pc}       							@ restora dados da pilha e sai da subrotina
    
   
/***************************************************************************************/
@ Rotina que extrai os digitos de um numero
@ Inputs:
@ 	r0 - numero a ser extraido os digitos

_extract_digits:
	push {r4-r11, lr}									@ salva variaveis na pilha
	ldr r4, =digits_array 								@ r4 recebe ponteiro para o inicio do vetor
	mov r6, r0											@ salva r0 em variavel temporaria
	
	ed_loop:
 		mov r5, r6 mod #10								@ r5 recebe r6 mod 10
 		str	r5, [r4], #4								@ guarda r5 no vetor e incrementa r4
 		udiv r6, r6, #10								@ divide r6 por 10
 		cmp r6, #0										@ compara r6 e zero
 		bne	ed_loop										@ se for diferente, continua dividindo
		
    pop {r4-r11, pc}       								@ restora dados da pilha e sai da subrotina
    
/***************************************************************************************/
@ Rotina que gera o maior numero inteiro a partir do vetor de digitos
@ Output:
@	r0 - numero gerado

_generate_max_number:
	push {r4-r11, lr}									@ salva variaveis na pilha

	pop {r4-r11, pc}       								@ restora dados da pilha e sai da subrotina

/***************************************************************************************/
@ Rotina que gera o menor numero inteiro a partir do vetor de digitos
@ Output:
@	r0 - numero gerado

_generate_min_number:
	push {r4-r11, lr}									@ salva variaveis na pilha
	mov r0, #0											@ inicializa o numero a ser gerado
	ldr r4, =digits_array								@ r4 recebe ponteiro para inicio do vetor
	ldr r5, =M											@ r5 recebe apontador para numero de digitos (tamanho do vetor)
	ldr r5, [r5]										@ r5 recebe numero de digitos
	
	gmn_loop:
		ldr	r6, [r4], #4								@ r6 recebe elemento do vetor (ja eh o menos significativo)
		mul r0, #10										@ multiplica r0 por 10
		add r0, r0, r6									@ resultado final: r0 = r0*10 + r6
		sub r5, #1										@ decrementa contador r5
		cmp r5, #0										@ compara r5 e 0
		bne gmn_loop									@ se forem diferentes, continua o processo

    pop {r4-r11, pc}       								@ restora dados da pilha e sai da subrotina
    
/***************************************************************************************/
@ Rotina que insere um elemento nao repetido em um vetor  
@ Inputs:
@	r0 - ponteiro para inicio do vetor
@	r1 - tamanho do vetor
@	r2 - numero magico a ser inserido
    
_add_kaprekar_in_array:
	push {r4-r11, lr}									@ salva variaveis na pilha
	bl _sort_asc										@ ordena vetor de kaprekar
	ldr r4, r0			                                @ r4 recebe ponteiro para inicio do vetor de kaprekar
	mov r5, r1											@ r5 recebe tamanho do vetor de kaprekar
	cmp r5, #0											@ compara r5 e zero
	beq add_at_end										@ se for igual, insere no vetor
	
	akia_loop:
		ldr r6, [r4], #4                      			@ carrega elemento do vetor
		cmp r2, r6										@ compara r2 e r6
		beq akia_end									@ se for igual, nao insere
		blt akia_insert									@ se r2 for menor, ja pode inserir
		sub r5, #1										@ decrementa tamanho do vetor
		cmp r5, #1										@ compara r5 e 1
		bne akia_loop									@ continua a procurar
		
	akia_insert:
		cmp r5, #1										@ compara r5 e 1
		beq add_at_end									@ se for igual, adiciona valor no fim
		ldr r6, [r4], #4                      			@ carrega elemento do vetor
		sub r5, #1										@ decrementa tamanho do vetor
		b akia_insert									@ continua a chegar no fim do vetor
		
		add_at_end:
			ldr r6, [r4], #4                      		@ carrega elemento do vetor
			srt r2, [r6]								@ guarda valor no vetor de kaprekar
	
	akia_end:
		pop {r4-r11, pc}       							@ restora dados da pilha e sai da subrotina

/***************************************************************************************/
@ Rotina que ordena um vetor. Baseada no algoritimo bubble-sort
@ Inputs:
@	r0 - ponteiro para inicio do vetor
@	r1 - tamanho do vetor

_sort_asc:
	push {r4-r11, lr}									@ salva variaveis na pilha
	cmp r1, #1                                          @ caso vetor possua tamanho 1
	ble end_sort                                      	@ encerra-se a rotina

	sub r5, r1, #1                                      @ numero de comparacoes: n-1
 	ldr r4, r0			                                @ r4 recebe ponteiro para inicio do vetor
 	mov r6, #0                                         	@ r6 eh setado quando ocorre swap

	loop_start:
 		ldr r7, [r4], #4                      			@ carrega elemento do vetor
 		ldr r8, [r4]                                    @ carrega proximo elemento
 		cmp r7, r8                                      @ compara ambos
 		ble no_swap                                   	@ se o segundo for maior, nao realiza o swap

 	mov r6, #1                                       	@ seta flag, pois ocorrera swap
 	sub r4, r4, #4                          			@ reseta ponteiro
 	swp r8, r8, [r4]                                   	@ realiza swap
 	str r8, [r4, #4]!                         			@ r8 recebe endereco incrementado
 	
	no_swap:
 		subs r5, r5, #1                                 @ contador decrementado
		bne loop_start                                  @ restarta loop caso seja preciso

	end_inner:
 		cmp r6, #0                                      @ verifica flag
 		beq end_sort                                    @ finaliza rotina caso flag nao estiver setada

 	mov r6, #0                                         	@ limpa flag
 	ldr r4, =digits_array                               @ r4 recebe ponteiro para inicio do vetor
 	sub r5, r1, #1                                      @ reseta contador
 	b loop_start                                       	@ intera novamente

	end_sort:
        pop {r4-r11, pc}       							@ restora dados da pilha e sai da subrotina
              
/***************************************************************************************/
@ Rotina que realiza o scan do valor de M

_scan:
    push {r0-r2, lr}    								@ salva variaveis na pilha
    ldr r0, =inmsg  									@ r0 recebe mensagem a ser exibida no monitor
	bl printf											@ printa-se a mensagem
	ldr r1, =M											@ r1 recebe constante M
	ldr r0, =scan_format								@ r0 recebe formato do scanf
	bl scanf											@ chama rotina que le o valor
	ldr r2, =M											@ r2 recebe ponteiro para M
	srt r0, [r2]										@ armazena em M o valor lido no scanf
    pop {r0-r2,pc}										@ restora dados da pilha e sai da subrotina
    
/***************************************************************************************/
@ Rotina que imprime o vator de kaprekar

_print_kaprekar_array:
	push {r0-r2, lr}    								@ salva variaveis na pilha
	ldr r0, =prtmsg										@ r0 recebe mensagem a ser printada
	ldr r2, =kaprekar_array_size						@ carrega em r2 ponteiro do tamanho do vetor de kaprekar
	ldr r2, [r2]										@ r2 recebe tamanho do vetor de kaprekar
	cmp r2, #0											@ compara r2 e 0
	beq pka_end											@ se for igual, nao printa nada
	ldr r3, =kaprekar_array			                    @ r3 recebe ponteiro para inicio do vetor de kaprekar
	
	pka_loop:
		ldr r1, [r3], #4                      				@ r1 recebe elemento do vetor
		bl printf											@ printa valor
		sub r2, #1											@ subtrai 1 do contador
		cmp r2, #0											@ compara r2 e 0
		bne pka_loop										@ se nao forem iguais, continua a interar

	pka_end:
   		pop {r0-r2,pc}									@ restora dados da pilha e sai da subrotina
        
/***************************************************************************************/
@ Secao de formatacoes
        
	scan_format:		.asciz "%d"
	inmsg: 				.asciz "\nInsira o número de dígitos M: "
	prtmsg:				.asciz "\n %d \n"

/***************************************************************************************/
@ Secao de dados

.data
.align	

	steps:				.word 8							@ numero de passos do algoritimo
	M: 					.word 0							@ numero de digitos
	kaprekar_qtd:    	.word 0							@ numeros magicos encontrador
	kaprekar_array_size .word 0							@ tamanho do vetor de kaprekar
	kaprekar_array:		.skip 20						@ vetor que guarda os numeros magicos encontrados
	digits_array:		.skip 20						@ vetor que guarda os digitos do numero analisado
	
.end